const express = require('express');
const app = express();
const hbs = require('hbs');
require('./hbs/helpers');

const port = process.env.PORT || 3000;

app.use(express.static(__dirname + '/public'));

//Express hbs engine
hbs.registerPartials(__dirname + '/views/partials');
app.set('view engine', 'hbs');

app.get('/', (req,res) => {
    res.render('home',{
        name: 'sebAStian smITh'
    });
});

app.get('/about', (req,res) => {
    res.render('about');
});

app.listen(port, () => {
    console.log(`Listen request for port:${port}`);
});